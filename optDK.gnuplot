load 'DK_formulas.inc'

set term epslatex size 8cm,4cm

lm = 6 # Left margin
rm = 5 # Right margin
gap = 1 # Distance between the plots

#
# Plot Δopt, Kopt
#
set out "optDK.tex"

set rmargin rm
set lmargin lm
set bmargin 0.5*gap

set label "\\Subref{figOptDK}" at screen -0.04, 0.9

set xtics format " "
set y2tics add (gprintf("%.3f", DmaxSNR) DmaxSNR,\
			gprintf("%.3f", KmaxSNR) KmaxSNR) \
		format " "
set ytics format "%5g" # Align with the bottom plot tics

unset key

set label "\\footnotesize{Optimal $\\Delta$}" at 14, 1.03
set label "\\footnotesize{Optimal $K$}" at 14, 0.7

unset xlabel
set ylabel "$\\Delta$, $K$" offset 2,0
plot [1:25] 'DK.dat' index 0 using 1:2 with lines lc 1,\
	'DK.dat' index 1 with lines lc 0,\
	DmaxSNR lc 2,\
	'DK.dat' index 0 using 1:(K($2)) with lines lc 1,\
	'DK.dat' index 1 using 1:(K($2)) with lines lc 0,\
	KmaxSNR lc 2

reset

#
# Plot Fmax and Fmax-FmaxSNR
#
set out "optDK_Fdiff.tex"

set rmargin rm
set lmargin lm
set tmargin 0.5*gap

set label "\\Subref{figDKFdiff}" at screen -0.04, 0.96

unset key

set format y "$%g\\%%$" # Show fidelity in percents
set format y2 "$%g\\%%$" # Show fidelity in percents

set xlabel "$\\Tm$"

set xtics add ("1" 1)
set ylabel "$F_\\text{max}$" offset 2,0
set ytics nomirror
set y2label "$F_\\text{max} - F_\\text{max. SNR}$" offset -0.1
set y2tics
set tics front

set label "$F_\\text{max}$" at 10,87 rotate by 10
set label "$F_\\text{max} - F_\\text{max. SNR}$" at 8.5,55 rotate by -5

plot [1:25]\
	'DK.dat' using ($1/2):($3*100) lc rgb "#eeeeee" w filledcurves x2,\
	'DK.dat' using 1:(($3 - F(DmaxSNR, to_nofactor($1)))*100)\
		lw 9 lt 1 lc rgb "white" with filledcurves x1 axes x1y2,\
	'DK.dat' using 1:(($3 - F(DmaxSNR, to_nofactor($1)))*100)\
		lc 2 with lines axes x1y2,\
	'DK.dat' using 1:($3*100) with lines lc 1,\
	'DK.dat' using ($1/2):($3*100) with lines lc rgb "gray" lt 1 lw 1
