xs = 0.001:0.01:10;

printf "Calculating D of X maximizing SNR\n"
ds = findmaxSNRds(xs);

xsds = horzcat(xs', ds');
save -ascii maxsnr.dat xsds
