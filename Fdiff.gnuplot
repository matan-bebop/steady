set xlabel "$X$"

set ylabel "$F^\mathrm{max} - F^{max. SNR}$$

set ytics nomirror
set y2label "F^\mathrm{max}"
set y2tics

plot 'Fdiff.dat' using 2:4, 'Fdiff.dat' using 2:4 smooth csplines,\
	'Fdiff.dat' using 2:5 axes x1y2, 'Fdiff.dat' using 2:5 with lines axes x1y2

pause -1
