source formulas.m

logarithmic = @(from, to, N) logspace(log10(from), log10(to), N)

printf "Calculating Ds maximizing F at X=1.0\n"
range = horzcat(logarithmic(0.1,1,10), 1.1:0.1:1.5, 1.51:0.01:2.5, 2.6:0.1:50);
tsdsFs = findmaxFds(1.0, range);
save_with_jumps("DmaxAtX1.dat", tsdsFs);

printf "Calculating Ds maximizing F at X=9\n"
%tsdsFs = findmaxFds(9, 0.1:0.1:30); % this fails to find the maximum somewhere
				     % after t = 30
tsdsFs = findmaxFds_smart(9, 0.1:0.1:50); % so use a better algorithm
save_with_jumps("DmaxAtX9.dat", tsdsFs, 9);
