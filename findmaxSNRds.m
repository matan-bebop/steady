function ds = findmaxSNRds(xs)
	f = @(d,x) (d+x)*((d-x)^2 + 1)^(3/2) - (d-x)*((d+x)^2 + 1)^(3/2);

	i = 1; for x = xs
		ds(i++) = fzero(@(d) f(d,x), [x, sqrt(x^2 + 1)]);
	endfor
endfunction
