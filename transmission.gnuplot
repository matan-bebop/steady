load 'colors.inc'

mean = 20 # Mean photon count in resonance
height = 1 + (1+0.5)/sqrt(mean)
halfwidth = 3 

set term epslatex color size 3.5cm,2.5cm
set out 'transmission.tex'

set bmargin 1
set tmargin 1
set lmargin 1
set rmargin 1

set multiplot

unset key
set border 1 front
set yzeroaxis lt -1

set xtics nomirror
set ytics axis

set style line 777 lc rgb "white" 
set border ls 777
set xtics ("\\footnotesize$\\omega_{\\dr\\res}$" halfwidth) offset 0,0.5
set ytics ("\\footnotesize{$\\overline n$}" height) offset 0.5,0
plot [-halfwidth:halfwidth][0:height] 0 lc rgb "white"
set border ls -1

set label "\\Subref{figTransmission}" at -halfwidth,height 

set arrow from 0,0 to 0,height lt -1 front
set arrow from 0,0 to halfwidth,0 lt -1 front

set xtics ("\\footnotesize$0$" 0,\
		"\\footnotesize$g\\lambda$" 1,\
		"\\footnotesize$-g\\lambda\\quad$" -1)
set ytics ("" 1)

set arrow from -1,0 to -1,1 nohead lt 2 lc rgb "black" front
set arrow from 1,0 to 1,1 nohead lt 2 lc rgb "black" front
set arrow from -0.5*halfwidth,1 to 1,1 nohead lt 2 lc rgb "black" front
set label "\\footnotesize$|\\alpha^\\mathrm{res}|^2$" at -1.1*halfwidth,1

set samples 1000

lorentzian(x) = 1/(x**2 + 1)
from(x) = lorentzian(x) - sqrt(lorentzian(x)/mean)
to(x) = lorentzian(x) + sqrt(lorentzian(x)/mean)

set style fill transparent solid 0.4
plot [-halfwidth:halfwidth][0:height]\
	'+' using 1:(from($1-1)):(to($1-1))\
			lc rgb upcolor with filledcurves closed,\
	'+' using 1:(from($1+1)):(to($1+1))\
			lc rgb downcolor with filledcurves closed,\
	lorentzian(x - 1) lt 1 lw 3 lc rgb upcolor,\
	lorentzian(x + 1) lt 1 lw 3 lc rgb downcolor

unset multiplot
