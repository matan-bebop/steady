to_nofactor = @(t) t * sqrt(27)/2;
from_nofactor = @(t_nf) t_nf * 2/sqrt(27);

tsdsFs = findmaxFdsks_smart(to_nofactor(1:0.1:50));
tsdsFs(:,1) = from_nofactor(tsdsFs(:,1));
save_with_jumps("DK.dat", tsdsFs);
