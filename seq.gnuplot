set term epslatex size 5.1cm,2.5cm
set out 'seq.tex'

set border 1
set xtics nomirror
set xtics out
unset ytics
unset key

set samples 500

set rmargin 1
set lmargin 1

piPulse(x) = exp(-x**2)
piPulseHalfWidth = 2.5*exp(0)

driveEnvelope(x) = 1 + tanh(x-2.7)

t0 = 10
tm = 4

from = -2*piPulseHalfWidth
to = (t0+tm)*1.3

set multiplot layout 2,1 

set label "\\Subref{figSeq}" at from,2 

set tmargin 1
set bmargin 0
set xtics (" " 0, " " t0, " " t0+tm)
set label "\\footnotesize{drive amplitude}" at t0/2, 0.7 front
plot [t=from:to]\
	driveEnvelope(t) with filledcurves x1 lc rgb "pink",\
	driveEnvelope(t) lc rgb "red" lt 1
unset label

set tmargin 0
set bmargin 1
set arrow from 0,0 to to,0 lt -1 front
set label "\\footnotesize$t$" at 0.95*to,character 0.23
set label "\\footnotesize{control}" at 0,0.3
set label "$\\pi$" at -1.2*piPulseHalfWidth, 0.5 front
set xtics ("\\footnotesize$0$" 0,\
			"\\footnotesize$t_0$" t0,\
			"\\footnotesize$t_0\\!+\\!\\tm$" t0 + tm) offset 0,0.5
plot [t=from:to][:1.1]\
	piPulse(t + piPulseHalfWidth) with filledcurves x1 lc rgb "pink",\
	piPulse(t + piPulseHalfWidth) lc rgb "red" lt 1

unset multiplot
