paper.pdf: Makefile paper.tex cqed.bib jj_sources.bib common_sources.bib measurement.bib counters.bib array.bib poznachennya.tex fizychni_komandy.tex maxsnr.tex distributions.tex distributions-small-kth.tex diagram.tex Dmax.tex optDK.tex optDK_Fdiff.tex transmission.tex seq.tex estimations.tex FDK.tex
	latex paper
	bibtex paper
	latex paper
	latex paper
	dvipdf paper.dvi

maxsnr.tex: maxsnr.dat snr.gnuplot
	gnuplot4 snr.gnuplot

maxsnr.dat: maxsnr.m findmaxSNRds.m
	octave maxsnr.m

DmaxAtX1.dat DmaxAtX9.dat: maxF.m findmaxFds.m findmaxFds_smart.m type1_tips.m type2_tips.m formulas.m save_with_jumps.m
	octave maxF.m

distributions.tex distributions-small-kth.tex: distributions.gnuplot poisson.gnuplot formulas.inc 
	gnuplot4 distributions.gnuplot

Dmax.tex: Dmax.gnuplot formulas.inc DmaxAtX1.dat DmaxAtX9.dat colors.inc
	gnuplot4 Dmax.gnuplot

DK.dat: optDK.m findmaxFdsks_smart.m DK_tips.m save_with_jumps.m
	octave optDK.m

optDK.tex optDK_Fdiff.tex: DK.dat optDK.gnuplot
	gnuplot4 optDK.gnuplot

transmission.tex: transmission.gnuplot
	gnuplot4 transmission.gnuplot

seq.tex: seq.gnuplot
	gnuplot4 seq.gnuplot

FDK.tex: FDK.gnuplot
	gnuplot4 FDK.gnuplot

diagram.tex: diagram.hs
	ghc diagram.hs
	./diagram -r -w 227 | grep -v pgftransformscale > diagram.tex
	# 227bp = 8cm; delete lines with \pgftransformshift to scale not text

run: paper.pdf
	mupdf paper.pdf
