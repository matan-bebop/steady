-- vi :set ts=4
-- vim :set sw=4 sts=4 et
 
{-# LANGUAGE NoMonomorphismRestriction #-}
{-# LANGUAGE FlexibleContexts #-}

import Diagrams.Prelude
import Diagrams.Backend.PGF.CmdLine

boxEnvelope w h = withEnvelope (rect w h :: D V2 Double)

labeled' w h dir str a =  beside dir a (label str)
    where label s = text s # boxEnvelope w h

labeledThinT = labeled' 0 labelSide unitY

labelSide = 1.5
labeled = labeled' labelSide labelSide

labeledT = labeled unitY
labeledR = labeled unitX
labeledB = labeled unit_Y
labeledL = labeled unit_X

node = circle 0.1 # fc black

whiteCircle = fc white circle
whiteEllipseXY = fc white ellipseXY

twoHeadsArrow = (with & arrowTail .~ dart' & arrowHead .~ dart)
interactionArrow = (with & gaps .~ (normalized 0.01) & arrowShaft .~ shaft)
    where shaft = cubicSpline False $ map p2 [(0, 0), (0.5, 0.2), (1, 0)]

dashed = dashingN [0.01, 0.01] 0

plot fun xs = cubicSpline False pts
    where pts =  map p2 (zip xs $ map fun xs)

sinusPlot from to = cubicSpline False pts # centerX # scaleX (1/(to-from))
    where pts =  map p2 (zip xs $ map sin xs)
          xs = [from, from+step .. to] ++ [to]
          step = pi/5

ground = (  vrule 0.7  
            === 
            hrule 1
            ===
            strutY 0.15
            === 
            hrule 0.6
            ===
            strutY 0.15
            === 
            hrule 0.2
        )

cavity w k1 k2 = (hrule 6 # translateY 1.5) # labeledT w
                 <> (hrule 6 # translateY (-1.5))
        <> (cap1 # labeledThinT k1 # translateX (-3))
        <> (cap2 # labeledThinT k2 # translateX 3)
    where cap1 = (node <> whiteEllipseXY 0.5 1.5) # boxEnvelope 0 3 
          cap2 = (arc yDir ((-0.5) @@ turn)
                    <> arc yDir (0.5 @@ turn) # dashed)
                 # scaleX 0.5 # scaleY (3/2)

qubit w = (hrule 1 # labeledR "$\\ket{\\uparrow}$" # translateY 1
        <> hrule 1 # labeledR "$\\ket{\\downarrow}$" # translateY (-1)
        <> arrowBetween' twoHeadsArrow ((-0.2) ^& 1) ((-0.2) ^& (-1))
                # labeledL w)
            # center

generator = sinusPlot (-pi) pi # scaleY 0.3 <> whiteCircle 1

circulator = arrowBetween' (with & arrowShaft .~ shaft) b e <> whiteCircle 1
    where shaft = (arc xDir (0.5 @@ turn)) :: Trail V2 Double
          b = p2(0.5, 0)
          e = p2(-0.5, 0)

detector = (vrule 2 <> (arc yDir $ -0.5 @@ turn)) # center

cavityLine = strutX 1 ||| generator # labeledT "$\\omega_\\dr$"
        ||| (beside unitY (hrule 3) driveWave)
        ||| (standingWave # named "mode"
             <> cavity "$\\omega_\\res$" "$\\gets\\kappa/2$" "$\\kappa/2\\to$")
        ||| hrule 2 ||| detector # labeledThinT "$\\eta$" ||| strutX 1 
    where driveWave = (sinusPlot 0 (6*pi) <> arrowBetween (0.5 ^& 0) (1 ^& 0))
                           # scaleY 0.3 # centerX # lc red
                    === strutY 0.2
          standingWave = sinusPlot 0 pi # lc red # dashed # scaleX 6

qubitLine = qubitControl ||| qubitInFrame # named "qubit"
    where qubitInFrame = node # translateX (-2.2)
                         <> circle 0.2 # translateY 1 # lc red # fc red
                         <> qubit "$\\omega_\\q$" <> circle 2.2 # fc white
          qubitControl = generator ||| (hrule 2.5 
                                           === piPulse # lc red # fc pink)
          piPulse = strutY 0.2 === ((text "$\\pi$"
                    <> (trail # closeLine # strokeLoop # lwN 0
                        <> trail # strokeLine) # center # scaleX 0.5) # snugR
                    <> arrowBetween (0 ^& 0) (0.8 ^& 0)) # centerX
              where trail :: Trail' Line V2 Double
                    trail = plot (\x -> exp $ -x*x) [-2, -1.8 .. 2]

cqedDiagram = (qubitLine # alignR # translate (12 ^& (-2)) <> cavityLine 
               <> text "$g$" # translate (8.3 ^& 0.5))
                    # coupling "qubit" (0.29 @@ turn) "mode" (0.25 @@ turn)
    where coupling o1 phi1 o2 phi2 
                = connectPerim' interactionArrow o1 o2 phi1 phi2
                  . connectPerim' interactionArrow o2 o1 phi2 phi1

figureLabel = text "\\Subref{figSystem}" # translateY 2.5

main = defaultMain (cqedDiagram <> figureLabel)
