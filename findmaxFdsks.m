function tsdsKs = findoptdsks(Ts=0.1:0.1:100, ds=1e-3:1e-3:10, Ks=1e-3:1e-3:10)
	pkg load gsl
	
	nup = @(d,K,T) T .* K.^3 ./ ((d-1).^2 + K.^2);
	ndown = @(d,K,T) T .* K.^3 ./ ((d+1).^2 + K.^2);
	nth_continous = @(d,K,T) (nup(d,K,T) - ndown(d,K,T)) ...
							./ log(nup(d,K,T) ./ ndown(d,K,T));
	nth = @(d,K,T) ceil(nth_continous(d,K,T));

	Dlo = sqrt((1+sqrt(5))/2); Klo = 3*(sqrt(5) - 1)/2;
	Dhi = @(k) sqrt(k^2 + 1);
	
	F = @(d,K,T) gamma_inc_Q(nth(d,K,T), ndown(d,K,T)) ...
			- gamma_inc_Q(nth(d,K,T), nup(d,K,T));

	tsdsKs = [];
	for T=Ts
		maxF = 0;
		for K=Ks
			[thisF, ithisD] = max(F(ds,K, T));
			if(thisF > maxF)
				maxF = thisF;
				ioptD = ithisD;
				optK = K;
			endif
		endfor
		tsdsKs = vertcat(tsdsKs, [T, ds(ioptD), optK, maxF]);
	endfor
endfunction

