# Functions on D only: K is set to be optimal

Iup(d) = (d+1) * sqrt(d**2 - 1) / (2*d + 1)
Idown(d) = (d-1) * sqrt(d**2 - 1) / (2*d - 1)
nth_continuous(d,t) = t*(Iup(d) - Idown(d)) / log(Iup(d)/Idown(d))
nth(d,t) = ceil(nth_continuous(d,t))

F(d,t) = igamma(nth(d,t), t*Iup(d)) - igamma(nth(d,t), t*Idown(d))
K(d) = sqrt(3 * d**2 - 3)

to_nofactor(t) = t * sqrt(27.)/2.

DmaxSNR = sqrt(5.)/2.
KmaxSNR = sqrt(3.)/2.

# Explicit D, K dependence

IupE(d,k) = k**3 / ((d-1)**2 + k**2)
IdownE(d,k) = k**3 / ((d+1)**2 + k**2)
nth_continuousE(d,k,t) = t*(IupE(d,k) - IdownE(d,k)) / log(IupE(d,k)/IdownE(d,k))
nthE(d,k,t) = ceil(nth_continuousE(d,k,t))

FE(d,k,t) = igamma(nthE(d,k,t), t*IupE(d,k)) - igamma(nthE(d,k,t), t*IdownE(d,k))

