set samples 2*range + 3
set style fill noborder solid 0.7
set ylabel "$P$" offset 1.5,0
set xlabel "$n$" offset 0,0.3
set x2range [-0.5:range + 0.5]
set x2tics in add ("$n_\\mathrm{th}$" kth(D,X,T)) format " "
set xtics out nomirror
set arrow front nohead lc rgb "black" lt 0 lw 4 \
	from kth(D,X,T),0 to kth(D,X,T), h

set style line 1 lc rgb downcolor lw 10 lt 1
set style line 2 lc rgb upcolor lw 5 lt 1

at_integers(val, x) = (x == ceil(x))? val : 1/0

round(x) = x - floor(x) < 0.5 ? floor(x) : ceil(x)

min(x,y) = (x<y)? x:y
minP(d,x, t, k) = min(P(d,x, t, k), P(d,-x, t, k))

plot [k = -0.5 : range + 0.5][0:h]\
	minP(D,X, T, round(k)) with fillsteps fillstyle solid lc rgb "gray"\
		t"$1-F$",\
	at_integers(P(D,X, T, k), k) with impulses ls 1 \
		t"$P_\\downarrow$",\
	at_integers(P(D,-X, T, k), k) with impulses ls 2 \
		t"$P_\\uparrow$"
unset arrow

set linetype 1 lc rgb upcolor lw 1
set linetype 2 lc rgb downcolor lw 1
