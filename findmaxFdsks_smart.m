function tsdsFs = findmaxFdsks_smart(ts, dupper=3, first_th=1)
	pkg load gsl
	source DK_formulas.m

	dmax_at = @(t,th) fzero(@(d) dFdD(d,t,th), [1.01,dupper]);

	th = first_th;

	dmax_at_th = dmax_at(ts(1), th);
	tsdsFs = [ts(1), dmax_at_th, F(dmax_at_th, ts(1))];
   	tprev = ts(1);

	for t = ts(2:end)
		dmax_at_th = dmax_at(t, th);
		dmax_at_next_th = dmax_at(t, th+1);
		Fmax_at_th = F(dmax_at_th, t);
		Fmax_at_next_th = F(dmax_at_next_th, t);
		% Determine if we are to increase th
		if (Fmax_at_th < Fmax_at_next_th)
			[tjump, d1, d2]...
				= DK_tips(th, [(t+tprev)/2, dmax_at_th, dmax_at_next_th]);
			Fjump = F(d1, tjump);
			tsdsFs = vertcat(tsdsFs, [tjump, d1, Fjump],...
								[tjump, d2, Fjump]); % Store the switch
			++th;
			dmax_at_th = dmax_at_next_th;
			Fmax_at_th = Fmax_at_next_th;
		endif
		tsdsFs = vertcat(tsdsFs, [t, dmax_at_th, Fmax_at_th]);
		tprev = t;
    endfor;
endfunction
