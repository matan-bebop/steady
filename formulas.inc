I(d,x) = 1./((d+x)**2 + 1)

kth_continous(d,x,t) = t * (I(d,-x) - I(d,x))/log(I(d,-x)/I(d,x))
kth(d,x,t) = ceil(kth_continous(d,x,t))
F(d,x,t) = -igamma(kth(d,x,t), t*I(d,x)) + igamma(kth(d,x,t), t*I(d,-x))
Fpart(kth,d,x,t) = sum [n=0:kth-1] t**n * (-I(d,-x)**n * exp(-t*I(d,-x)) + I(d,x)**n * exp(-t*I(d,x))) / gamma(n+1)
Finf(t) = 1 - exp(-t)

P(d,x, t, k) = (k >= 0)? (t*I(d,x))**k / gamma(k+1) * exp(-t*I(d,x)) : 1/0

# Minimal average number of photons incident on detector to consider the 
# photocounting statistics to be gaussian
n_gauss = 3
# Minimal time of integration to consider the statistics to be gaussian
t_gauss(d,x) = n_gauss * ((d+x)**2 + 1)

Pgauss(d,x,t, k) = 1/sqrt(2*pi*t*I(d,x)) * exp(-(k-t*I(d,x))**2/2/(t*I(d,x)))
kthgauss(d,x,t)=sqrt(I(d,x)*I(d,-x)*t \
			* (t + log(I(d,-x)/I(d,x))/(I(d,-x)-I(d,x))))
xth(d,x,t) = (kthgauss(d,x,t) - t*I(d,x)) / sqrt(2*t*I(d,x))
Fgauss(d,x,t) = (t > t_gauss(d,x))?\
	0.5*(-erf(xth(d,-x,t)) + erf(xth(d,x,t))) : 1/0

Dlo(x) = sqrt((2*sqrt(x**4 + x*x + 1) + x*x - 1)/3)
Dhi(x) = sqrt(x*x + 1)
