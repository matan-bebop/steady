upcolor = "olive"; downcolor = "blue"

DoptColor = "red"

set macros

n_styles = 0
NEWSTYLE = "n_styles= n_styles+1"

set style line Dopt=@NEWSTYLE lc rgb DoptColor lw 2 lt 1
set style line DoptJump=@NEWSTYLE lc rgb DoptColor lw 1 lt 0
set style line Dbounds=@NEWSTYLE lc rgb "black" lw 1 lt 2
set style line DoptSNR=@NEWSTYLE lc rgb "black" lw 3 lt 3

unset macros
