function tsds = findmaxFds(X, Ts=0.1:0.1:100, eps=1e-3)
	source formulas.m;

	tsds = [];

	Dlow = Dlo(X); Dhigh = Dhi(X);

	delta = eps * (Dhigh - Dlow);
	Ds=Dlow:delta:Dhigh;

	[fidelity, iD] = max(F(Ds, X, Ts(1)));
	tsds = vertcat(tsds, [Ts(1), Ds(iD), fidelity]);
	for T=Ts(2:end)
		[fidelity, iD] = max(F(Ds, X, T));
		Dprev = tsds(end,2); D = Ds(iD);

		Tprev = tsds(end,1);
		kthprev = kth(Dprev, X, Tprev);
		
		if(D < Dprev &&  kthprev+1 == kth(D, X, T)) % Check if it is a jump
			% Determine the jump type: it is either due to higher F avaliable
			% at the next kth (type-1 jump), or because there is simply no 
			% D maximizing F (type-2 jump).

			% This is a type-1 jump, if a maximum of F exists on [Dlo, Dhi]
			if(sign(dFdD(Dlow,X,T,kthprev))*sign(dFdD(Dhigh,X,T,kthprev)) < 0)
				[Tjump, D1, D2]...
					= type1_tips(X, kthprev, [(Tprev + T)/2, Dprev, D]);
				F1 = F2 = F(D1,X,Tjump);
			else % type-2 jump
				[Tjump,~,D2] = type2_tips(X, kthprev, Dlow, Dhigh);
				D1 = Dhigh;
				F1 = F(D1,X,Tjump); F2 = F(D2,X,Tjump);
			endif
			% Store the precise switch point
			tsds = vertcat(tsds, [Tjump, D1, F1], [Tjump, D2, F2]);
		endif
		tsds = vertcat(tsds, [T, D, fidelity]);
	endfor
endfunction
