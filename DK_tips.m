function [t, d1, d2] = DK_tips(th, seed)
% Returns [t, d1, d2] -- adjancent tips of two intervals of continuous Dopt
% with threshold  counts th and th+1. The last parameter, seed = [t, d1, d2] 
% sets the first guess for the solver.
	source DK_formulas.m
		
	y = @(v) [dFdD(v(2), v(1), th),...
				dFdD(v(3), v(1), th+1),...
				F(v(2), v(1)) - F(v(3), v(1))];

	[solution, ~, info] = fsolve(y, seed);
	if(info == 0)
		warning("Iteration limit exceeded while searcing for tip points");
	endif
	t = solution(1); d1 = solution(2); d2 = solution(3);
endfunction
