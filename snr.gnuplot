set bmargin 2
set rmargin 6.5

#
# Plot the graph showing the difference between the optimal and naive (D=X)
# detunings, the resulting SNR and the increase in SNR.
#

set out 'maxsnr.tex'
set term epslatex mono size 8.6cm,7cm

p(d,x) = 1/((d+x)**2 + 1)
m(d,x) = 1/((d-x)**2 + 1)
snr(d,x) = sqrt(m(d,x)) - sqrt(p(d,x))

set logscale y
set ytics format "$10^{%L}$" add ("1" 1)
set xlabel "$X$"
set ylabel "\\shortstack{$D^\\text{opt}-X$ \
	\\\\ $\\SNR^1 - \\SNR^1_{D=X}$}"

set ytics nomirror

unset log y2
set y2label "$\\SNR^1$" offset -1,0
set y2tics

unset key

set label "$D^\\text{opt}-X$" at 2,0.08 rotate by -17
set label "$\\SNR^1$" at  2,0.35 rotate by 21
set label "$\\SNR^1 - \\SNR^1_{D=X}$" at  1.5,0.01 rotate by -37

smart_diff(opt, naive) = (opt-naive < 0.01 && opt < 0.01)? 1/0 : opt-naive
plot [0:3.5][1e-4:1] \
	 'maxsnr.dat' using 1:($2-$1) smooth csplines, \
	 'maxsnr.dat' using 1:(snr($1, $2)) axes x1y2 smooth csplines, \
	 'maxsnr.dat' using 1:(smart_diff(snr($1, $2), snr($1,$1)))\
		 	axes x1y1 smooth csplines
