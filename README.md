steady
======

Contains the code for producing text and figures for the "Optimal conditions
for high-fidelity dispersive readout of a qubit with a photon-number-resolving
detector" paper published as [Physical Review A 93 (3), 032323](https://doi.org/10.1103/PhysRevA.93.032323).

The idea is to read out a qubit by continuousely probing it with a coherent-
state radiation and an ideal photon-number-resolving detector.

Note that this version is the same as the [latest manuscript](https://arxiv.org/abs/1512.08789) uploaded to ArXiv.
It contains several mistakes corrected, as compared to the journal version.

You can compile paper.pdf of the arXiv version of the paper simply by running

```
> make
```

That handles the calculations and produces the figures automatically. For that,
Gnuplot 4, Octave, and the GSL binding for Octave are required. The Makefile
recipe also produces the scheme diagram. For that, it requires the ghc Haskell
compiler, the Diagrams and the Diagrams-PGF Haskell libraries.

To install Gnuplot 4 in your home directory on Linux, you may try downloading
its source tarball, extracting it somewhere, and running:

	/configure --without-lua --prefix=/home/usersFWM/andrii/.local/
	make
	make install

Alternatively, you can use the updated Gnuplot 5 scripts from the `thesis`
branch. Note that some plots there are of bigger size.

All the details of the plotting and calculations can be figured out from 
`Makefile` and `paper.tex`. Here we only outline on the

Scripts performing calculations
===============================

To maximize the SNR
-------------------

`maxsnr.m` – a script to find the the drive-cavity detuning that maximizes
SNR in the photodetector signal in a reasonable range. Uses:

`findmaxSNR.m` with the function to calculate the optimal-SNR detuning for a
given dispersive pull

To maximize the fidelity
------------------------

`maxF.m` – a script that calculates the measurement times and the detunings to
maximize the measurement fidelity. Uses:

`findmaxFds.m` with the function to find the optimal detuning for a given
measurement duration and does not work for higher dimensionless dispersive pull
and dimensionless measurement time

`findmaxFds_smart.m` with the function to do the same, but works reliably for
the whole parameter range in the paper.

`optDK.m` – a script that calculates the optimal detuning and decay rate
using:

`findmaxFdsks.m` and `findmaxFdsks_smart` are similar to the functions used in
`maxF.m`, but in addition to the detuning they also vary the resonator decay
rate.

Several of the above scripts and functions use the following:

`type1_tips.m` with the function to find a discontinuity in the optimal detuning
that arises due to a higher fidelity available for another detuning for the
next threshold count

`type2_tips.m` with the function to find the same but for the case when there is
no more an optimal detuning for this threshold count 

`save_with_jumps.m` with the function to save the optimal parameters data so
that it is straightforward to plot discontinuities with Gnuplot

`formulas.m` contains several formulas to be included with `source` that can be
useful in finding the optimal detuning

`DK_formulas.m` similar but for finding the optimal detuning _and_ the resonator
decay rate

`DK_tips` with the function to find the discontinuity in the detuning and the
resonator decay rate
