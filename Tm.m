function Tm = Tm(Fgoal)
	source DK_formulas.m

	tsdsks = load_DK('DK.dat');
	
	d_poly = interp1(tsdsks(:,1), tsdsks(:,2), "pp");
	Tm = fzero(@(t) F(ppval(d_poly, t), to_nofactor(t)) - Fgoal, [1, 20]);
endfunction

function tsdsks = load_DK(filename)
	f = fopen(filename);
	if(!f)
		message = strcat('Error opening file ', filename)
		error message
	endif

	tsdsks = [];
	while 1
		l = fgetl(f);
		[tdk, count] = sscanf(l, '%f %f %f');
		if(count != 0)
			if(count < 3)
				break
			endif
			tsdsks = vertcat(tsdsks, tdk');
		endif
	endwhile

	fclose(f)
endfunction
	
