load 'DK_formulas.inc'
#Kopt(d) = sqrt(3 * d**2 - 3)

# Plot
set term epslatex color solid size 8cm,5.1cm
set out "FDK.tex"

set lmargin 2
set rmargin 0.5
set tmargin at screen 0.95
set bmargin at screen 0.25

set isosamples 100

unset surface
set view map

set contour
set cntrparam levels incremental 82,0.5,94.5

set pm3d
set palette defined (78 "blue", 93 "white", 95 "red")

set linetype 1 lc rgb 'black'
set linetype cycle 1

set tics out

set grid cb ls 1
set grid mcb ls 1
set mcbtics 4
set format cb "$%g\\%%$" # Show fidelity in percents
unset key

set xtics auto 0.1

set xlabel "$\\Delta$"
set ylabel "$K$" offset 0.7

D = 1.0933
K = K(D)
set label "\\footnotesize{95\\%}" at D+0.02,K+0.01 front
#set object circle at DmaxSNR,KmaxSNR front size 0.003 fc rgb "blue"
set style fill solid
set object circle at D,K front size 0.003 fc rgb "black"
splot [d=0.9:1.4][k=0.5:1.3] 100*FE(d,k,11.29)
