function [Tjump, Dfrom, Dto] = type2_tips(X, th_from, Dlow=0, Dhigh=0)
	source formulas.m

	if(Dlow == Dhigh)
		Dlow = Dlo(X); Dhigh = Dhi(X);
	endif

	Dfrom = Dhigh; 

	Tjump = th_from * log(I(Dfrom,-X)/I(Dfrom,X)) / (I(Dfrom,-X) - I(Dfrom,X));

	if(sign(dFdD(Dlow, X, Tjump, th_from+1))
			*sign(dFdD(Dhigh, X, Tjump, th_from+1)) < 0)
		% There is a maximum at next th; find it
		dmax_at = @(t,th) fzero(@(d) dFdD(d,X,t,th), [Dlow,Dhigh]);
		Dto = dmax_at(Tjump, th_from+1);
	else
		% No maximum; fall down to Dlow not reaching it however
		Dto = Dlow; % NOTE: It seems this case never occurs
	endif
endfunction
