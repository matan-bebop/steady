load 'formulas.inc'
load 'colors.inc'

set term epslatex color size 8.6cm,6.5cm
set out 'Dmax.tex'

unset key
set tics front

set rmargin 4.5
set lmargin 7
bottom_margin = 0.18 # screen
gap = 0.05 # screen
height = 0.72 # screen
k_bottom = 0.55 # relative height of the bottom plot

set xlabel "$\\taum$"
set ylabel "$D$" offset 2,2

set multiplot

set bmargin at screen bottom_margin
set tmargin at screen bottom_margin + k_bottom * height

set border 1+2+8

set xtics nomirror
set y2tics ("1.17" 1.17, "0.84" 0.84, "0.72" 0.72) format " "
set ytics autofreq 0.1
set label "\\footnotesize{$\\Dhi$ for $X=1$}" at 25, Dhi(1)-0.05
set label "\\footnotesize{$\\Dlo$ for $X=1$}" at 25, Dlo(1)-0.05
plot [0:48][0.96:1.43] 'DmaxAtX1.dat' index 0 with lines ls Dopt,\
			'DmaxAtX1.dat' index 1 with lines ls DoptJump,\
			1.17 ls DoptSNR,\
				Dlo(1) ls Dbounds, Dhi(1) ls Dbounds

set bmargin at screen bottom_margin + k_bottom * height + gap
set tmargin at screen bottom_margin + height + gap

set border 2+4+8
unset xtics
set x2tics format " "

unset xlabel
unset ylabel
unset y2tics
set ytics autofreq 0.04
set y2tics ("9.004" 9.004) format " "
set label "\\footnotesize{$\\Dhi$ for $X=9$}" at 4, Dhi(9)+0.015
set label "\\footnotesize{$\\Dlo$ for $X=9$}" at 4, Dlo(9)-0.02
plot [0:48][8.97:9.09] 'DmaxAtX9.dat' index 0 with lines ls Dopt,\
	'DmaxAtX9.dat' index 1 with lines ls DoptJump,\
	9.004 ls DoptSNR,\
	Dlo(9) ls Dbounds, Dhi(9) ls Dbounds,\
	'DmaxAtX9.dat' index 2 using ($1+$3):($2+$4):(0.35)\
		 with circles fs solid lc rgb "white" ,\
	'DmaxAtX9.dat' index 2 using ($1+$3):($2+$4):(0.35)\
		 with circles ls Dopt

unset multiplot

# Draw the gap decorations
set lmargin 5
set rmargin 1
set bmargin at screen bottom_margin + k_bottom * height
set tmargin at screen bottom_margin + k_bottom * height + gap
unset label
set border 0
unset tics
unset xlabel
unset ylabel
set samples 10
plot rand(0) lc rgb "gray" lw 2
