load 'formulas.inc'
load 'colors.inc'

key_height = 2
bm = 3

set rmargin 0.6

#
# Plot the probability distributions for a big measurement duration 
#
set term epslatex color size 8.6cm,6cm
set out 'distributions.tex'

set tmargin 2

D = 0.6; X = 0.15; T = 20

set label gprintf("\\Subref{figDistributions20Photons} $\\taum=%.0f$", T)\
			at screen 0, 1

set key outside below samplen 2

set multiplot
set bmargin bm + key_height
set lmargin 7
range = 30
h = 0.12

load 'poisson.gnuplot'

set samples 200
unset tics
unset label
unset xlabel; unset ylabel
set border 0
set style line 1 lc rgb downcolor lw 3 lt 2
set style line 2 lc rgb upcolor lw 3 lt 2
set linetype cycle 2
plot [k= -0.5 : range + 0.5][0:h]\
	Pgauss(D,X, T, k) ls 1 title "",\
	Pgauss(D,-X, T, k) ls 2 title ""

unset multiplot
set tics
set xlabel; set ylabel
set border

#
# Plot the dsitributions for a small time
#
set term epslatex color size 8.6cm,4cm
set out 'distributions-small-kth.tex'

T = 0.5

set label gprintf("\\Subref{figDistributionsHalfaPhoton} $\\taum=%.1f$", T)\
			at screen 0, 0.9

unset key
set lmargin 6
set tmargin 2.5
set bmargin bm

set multiplot
range = 3
h = 0.75

set xtics 1
set x2tics 1
set x2tics in add ("$n_\\mathrm{th}^\\mathrm{cont}$" kth_continous(D,X,T))\
	format " "
set ytics 0.2
set tics front
load 'poisson.gnuplot'

set samples 200
unset tics
unset label
unset xlabel; unset ylabel
set border 0
set style line 1 lc rgb downcolor lw 1 lt 1
set style line 2 lc rgb upcolor lw 1 lt 1
set linetype cycle 2
set arrow front nohead lc rgb "black" lt 0 lw 4 \
	from kth_continous(D,X,T), h\
		to kth_continous(D,X,T), P(D,X,T, kth_continous(D,X,T))
plot [k = -0.5 : range + 0.5][0:h]\
	P(D,X, T, k) ls 1 title "",\
	P(D,-X, T, k) ls 2 title ""

