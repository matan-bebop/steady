pkg load gsl

I = @(d,x) 1./((d+x).^2 + 1);

kth_continous = @(d,x,t) t .* (I(d,-x) - I(d,x)) ./ log(I(d,-x)./I(d,x));
kth = @(d,x,t) ceil(kth_continous(d,x,t));

F = @(d,x,t) gamma_inc_Q(kth(d,x,t), t*I(d,x)) ...
			- gamma_inc_Q(kth(d,x,t), t*I(d,-x));

% A function proportional to the derivative dF/dD
dFdD = @(d, x, t, th) (((d+x).^2 + 1)./((d-x).^2 + 1)).^(1+th)...
			- (d+x)./(d-x) .* exp(t.*(I(d,-x) - I(d,x)));

Dlo = @(x) sqrt((2*sqrt(x^4 + x*x + 1) + x*x - 1)/3);
Dhi = @(x) sqrt(x*x + 1);
