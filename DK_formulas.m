Iup = @(d) (d+1) * sqrt(d^2 - 1) / (2*d + 1);
Idown = @(d) (d-1) * sqrt(d^2 - 1) / (2*d - 1);
nth_continuous = @(d,t) t*(Iup(d) - Idown(d)) / log(Iup(d)/Idown(d));
nth = @(d,t) ceil(nth_continuous(d,t));
F = @(d,t) gamma_inc_Q(nth(d,t), t*Idown(d)) ...
		- gamma_inc_Q(nth(d,t), t*Iup(d));
% A function proportional to the derivative dF/dD
dFdD = @(d, t, th) (d+1) * exp(t*(Iup(d) - Idown(d)))...
					- (d-1) * (Iup(d)/Idown(d))^(1+th);

to_nofactor = @(t) t * sqrt(27.)/2.;

