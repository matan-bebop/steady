function maxs = max_diffFs(xs, ts)
	source formulas.m

	maxs = [];
	for x = xs
		dSNR = findmaxSNRds(x);
		ds = findmaxFds(x, ts);
		this_x_diffs = ds(:,3) - F(dSNR,x,ds(:,1));
		[this_max, idiff] = max(this_x_diffs);
		% Store [d, x, t, Fopt - FoptSNR, Fopt]
		maxs = vertcat(maxs, [ds(idiff,2), x, ds(idiff,1), this_max, ds(idiff,3)]);
	endfor		
endfunction

diffs = max_diffFs(0.1:0.1:2.5, 0.1:0.01:10);
plot(diffs(:,2), diffs(:,4));
pause
