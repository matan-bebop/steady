function tsds = findmaxFds_smart(x,ts, first_th=1)
	source 'formulas.m'

	dlower = Dlo(x); dupper = Dhi(x);
	dmax_at = @(t,th) fzero(@(d) dFdD(d,x,t,th), [dlower,dupper]);

	th = first_th;

	dmax_at_th = dmax_at(ts(1), th);
	tsds = [ts(1), dmax_at_th, F(dmax_at_th, x, ts(1))];
   	tprev = ts(1);

	for t = ts(2:end)
		% Determine if we are to increase th, due to either type of jump.
		% Check if we have an extremum at this t, th for Dlo < D < Dhi
		if(sign(dFdD(dlower, x, t, th))*sign(dFdD(dupper, x, t, th)) >= 0)
			% There could be an extremum at D = Dhi only;
			% thus it is a type-2 jump
			[tjump,~,dto] = type2_tips(x, th, dlower, dupper);
			% Store the switch
			tsds = vertcat(tsds, [tjump, dupper, F(dupper,x,tjump)],...
		   						[tjump, dto, F(dto,x,tjump)]);
			++th;
			dmax_at_th = dmax_at(t, th);
			Fmax_at_th = F(dmax_at_th, x, t);
		else
			dmax_at_th = dmax_at(t, th);
			Fmax_at_th = F(dmax_at_th, x, t);
			% Check if there is an extremum at th+1
			if(sign(dFdD(dlower, x, t, th+1)) != sign(dFdD(dupper, x, t, th+1)))
				dmax_at_next_th = dmax_at(t, th+1);
				Fmax_at_next_th = F(dmax_at_next_th, x, t);
				if (Fmax_at_th < Fmax_at_next_th)
					% Type-1 jump due to higher F avaliable at th+1
					[tjump, d1, d2]...
						= type1_tips(x, th, ...
							[(t+tprev)/2, dmax_at_th, dmax_at_next_th]);
					% Store the switch
					Fjump = F(d1, x, tjump);
					tsds = vertcat(tsds, [tjump, d1, Fjump],...
										[tjump, d2, Fjump]);
					++th;
					dmax_at_th = dmax_at_next_th;
					Fmax_at_th = Fmax_at_next_th;
				endif
			%else
			% If there is no extremum at th+1, F is maximum either at Dlo or
			% Dhi there. However, this cannot be, as Dlo < Dmax <= Dhi.
			endif
		endif
		tsds = vertcat(tsds, [t, dmax_at_th, Fmax_at_th]);
		tprev = t;
    endfor;
endfunction
