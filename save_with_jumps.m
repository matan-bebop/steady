function save_with_jumps(filename, tsdsFs, X=0)
	source formulas.m

	t = @(j) tsdsFs(j,1); D = @(j) tsdsFs(j,2); F = @(j) tsdsFs(j,3);

	f = fopen(filename, "w");
	format long e; split_long_rows(0);

	verticals = []; arrows = [];

	% Save (t, Dopt, Fmax) triples
	fdisp(f, tsdsFs(1,:));
	i = 1; while(++i <= length(tsdsFs))
		% Two Ds are stored for the same time in case of a jump only
		if(t(i) == t(i-1))
			fprintf(f, "\n"); % put a newline to indicate discontinuity
			% and remember the vertical line to use it further
			verticals = horzcat(verticals, [t(i), D(i-1), D(i)]');
			% Check if it is the type-2 jump to Dlo and X was specified
			if(X > 0 && D(i) == Dlo(X))
				% The lower bound is never reached;
				if(i < length(tsdsFs))
					% save coordinates of an arrow to indicate this
					arrows = horzcat(arrows, [t(i+1), D(i+1),... 
											t(i)-t(i+1), D(i)-D(i+1)]');
				else  % The point never reached is the last point;
					continue % don't write it out
				endif
			endif
			if(F(i-1) < F(i)) % happens in case of type-2 jumps
				% The point with lower F is never reached;
				% save coordinates of an arrow to indicate this
				arrows = horzcat(arrows, [t(i-2), D(i-2),... 
										t(i-1)-t(i-2), D(i-1)-D(i-2)]');
			endif
		endif
		fdisp(f, tsdsFs(i,:));
	endwhile

	fprintf(f, "\n\n"); % put 2 newlines to begin next data block

	% Save vertical lines connecting the two tips of adjancent continuos 
	% intervals
	for td1d2 = verticals
		fdisp(f, [td1d2(1), td1d2(2)]);
		fdisp(f, [td1d2(1), td1d2(3)]);
		fprintf(f, "\n"); % put a newline to indicate discontinuity
	endfor

	fprintf(f, "\n"); % there are 2 newlines in sum to begin next data block

	for arrow = arrows
		fdisp(f, arrow');
	endfor

	fclose(f);
endfunction
